## Step-by-step scripts

This directory contains the many scripts used to run a sample genome assembly.

They have been used 'manually', i.e. submitted one script at a time.

They can be the base to setup a complete pipeline using Nextflow glue.

One can use these scripts to setup Nextflow modules, i.e. the steps of the pipeline.

## Pipeline sketch

Altogether, these scripts execute these steps:

![Pipeline sketch](../assets/assemblify-sketch.png)
