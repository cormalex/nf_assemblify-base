process fastqc {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'lowmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag "QC_${fq_ch.baseName}"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/01a_fastqc",	mode: 'copy', pattern: '*fastqc.*'
    publishDir "${params.resultdir}/logs/fastqc",	mode: 'copy', pattern: 'fastqc*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'fastqc*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
        each(fq_ch)

    // Workflow output stream
    output:
        // results from this step will be available using 'qc_ch' name
        path("*fastqc.*"), emit: qc_ch
        // we also collect commands executed and log files
        path("fastqc*.log")
        path("fastqc*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    fastqc.sh $fq_ch fastqc_${fq_ch.baseName}.cmd >& fastqc_${fq_ch.baseName}.log 2>&1
    """ 
}



